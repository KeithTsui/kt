signature SYMBOL =
sig
    eqtype symbol
    val symbol : string -> symbol
    val name : symbol -> string

    type 'a table
    val empty : 'a table
    val enter : 'a table * symbol * 'a -> 'a table
    val look  : 'a table * symbol -> 'a option
end

structure Symbol :> SYMBOL = struct

(* A symbol is a tuple of name and id in symbol table *)
type symbol = string * int


exception Symbol

(*https://www.smlnj.org/doc/smlnj-lib/Util/str-HashTable.html*)
structure H = HashTable (* from $/smlnj-lib.cm *)

val nextID = ref 0
val sizeHint = 128

(* Use a hash table to store the mapping from symbol name to symbol id *)
val hashtable : (string, int) H.hash_table =
    H.mkTable(HashString.hashString, op = ) (sizeHint,Symbol)

(* get a symbol for a given name *)
fun symbol name =
    case H.find hashtable name
     of SOME id => (name, id)
      | NONE =>
        let
            val i = !nextID
        in nextID := i+1;
           H.insert hashtable (name,i);
           (name,i)
        end

(* get symbol name from a symbol *)
fun name(s, n) = s


(* Symbol Table which contains mappings from symbol to its binding *)
structure Table = IntMapTable(type key = symbol
                              fun hash(s,n) = n)

type 'a table= 'a Table.table

(* make an emptyp table *)
val empty = Table.empty

(* enter(table, symbol, bindings) *)
val enter = Table.enter

(* look(table, symbol) -> bindings *)
val look = Table.look



end (* structure Symbol *)
