functor CompilerFunctor (cg: CODEGEN) = struct

structure Frame : FRAME = cg.Frame
structure Tr = TranslateFunctor(Frame)
structure Semant = SemantFunctor(Tr)
structure F = Frame
structure A = Assem
structure RegAlloc = RegAllocFunctor(cg)

fun getsome (SOME x) = x

fun indent instrs =
    map (fn (i) =>
            case i of
                l as A.LABEL _ => l
              | A.OPER{assem,src,dst,jump} =>
                A.OPER{assem="\t"^assem,src=src,dst=dst,jump=jump}
              | A.MOVE{assem,dst,src} =>
                A.MOVE{assem="\t"^assem,src=src,dst=dst}
        ) instrs

fun tempname alloc temp =
    case Temp.Table.look(alloc,temp) of
        SOME(r) => r
      | NONE => Frame.temp_name temp

fun emitproc out (F.PROC{body,frame}) =
    let
        val IR = body
        val _ = print ("emit " ^ Symbol.name (Frame.name frame) ^ "\n")
        val linearizedIR = Canon.linearize IR
        val scheduledIR = Canon.traceSchedule(Canon.basicBlocks linearizedIR)

        (* ISel*)
        val MIR = List.concat(map (cg.codegen frame) scheduledIR)

        (* mark live-out registers of procedure *)
        val markedMIR = Frame.procEntryExit2 (frame, MIR)

        (* Register Allocation *)
        val (RegAllocMIR, alloc) = RegAlloc.alloc(markedMIR, frame)

        (* Prologue and Epilogue *)
        val {prolog=prolog, body=cookedMIR, epilog=epilog}
            = Frame.procEntryExit3(frame, RegAllocMIR)

        val indentMIR = indent cookedMIR

        val formated = Assem.format(tempname alloc)

        val printIRs =
         fn IRs => app (fn ir => Printtree.printtree(TextIO.stdOut, ir) ) IRs

        val printMIRs =
         fn MIRs => app (fn mir => Assem.show(mir) ) MIRs


    in
        print("\n\nIR tree: \n");
        Printtree.printtree(TextIO.stdOut, IR);

        print("\n\nIR Linearized: \n");
        printIRs linearizedIR;

        print("\n\nIR Scheduled: \n");
        printIRs scheduledIR;

        print("\n\nAfter ISel MIR: \n");
        printMIRs MIR;

        print("\n\n Live-outs marked MIR: \n");
        printMIRs markedMIR;

        print("\n\n Register Allocated MIR: \n");
        printMIRs RegAllocMIR;

        print("\n\n Pro-Epi MIR: \n");
        printMIRs cookedMIR;

        print("\n\n Indented MIR: \n");
        printMIRs indentMIR;

        print("\n\n prolog: \n");
        print(prolog);

        print("\n\n Epilog: \n");
        print(epilog);



        TextIO.output(out, prolog);

        app (fn i => TextIO.output(out,(formated i) ^ "\n")) indentMIR;

        TextIO.output(out, epilog)
    end

  | emitproc out (F.STRING(lab,s)) = TextIO.output(out,F.string(lab,s))

fun emitstr out (F.STRING(lab,str)) = TextIO.output(out,Frame.string(lab,str))

fun withOpenFile fname f =
    let val out = TextIO.openOut fname
    in (f out before TextIO.closeOut out)
       handle e => (TextIO.closeOut out; raise e)
    end

fun compile filename =
    let val absyn = Parse.parse filename
        val frags = (FindEscape.findEscape absyn;
                     Semant.transProg absyn)
        val (progs,strs) =
            List.partition
                (fn (x) => case x of
                               F.PROC(_) => true
                             | _ => false) frags
    in
        withOpenFile (filename ^ ".a")
                     (fn out =>
                         let
                             val print = fn str => TextIO.output(out, str)
                         in
                             print("\t .global main \n");
                             print("\t .extern printi \n");
                             (* LCC Assembly format*)
                             app (emitstr out) strs;
                             print("\n");
                             app (emitproc out) progs
                         end)
    end


fun main (cmd: string, args: string list) =
    let in app compile args; 0 end

end
