(* like IR in llvm after IRCodegen from AST *)
signature TREE = sig

    type label = Temp.label
    type size

    (* statements perform side-effects and control flow *)
    datatype stm = SEQ of stm * stm          (* stm1 followed by stm2 *)
                 | LABEL of label            (* as machine code address, like label of assembly *)
                 | JUMP of exp * label list  (* Transfer control to exp,
                                              * exp should be evaluated as one of label list  *)
                 | CJUMP of relop * exp * exp * label * label (* Conditional jump,
                                                               * exp1 relop exp2,
                                                               * true to label1, false to label2 *)
                 | MOVE of exp * exp         (* move exp2 to exp1, exp1 should be l-value
                                              * i.e. exp1 is of MEM or TEMP *)
                 | EXP of exp                (* evaluate exp and discard result  *)

         (* expression, stand for the computation of some value  *)
         and exp = BINOP of binop * exp * exp (* Binary Operation of two values *)
                 | MEM of exp                 (* the location of Memory at address of exp, i.e. Mem[exp] *)
                 | TEMP of Temp.temp          (* A tempory in abstract machine as a register in a real machine *)
                 | ESEQ of stm * exp          (* The stm is evaluated as side effect, and exp for a result *)
                 | NAME of label              (* Symbolic Constant, i.e. label of Assembly *)
                 | CONST of int               (* Integer Constant *)
                 | CALL of exp * exp list     (* A procedure call, i.e. CALL(f, args) == f(args) *)

         and binop = PLUS | MINUS | MUL | DIV
                     | AND | OR | XOR
                     | LSHIFT | RSHIFT | ARSHIFT

         and relop = EQ (* equal *)
                   | NE (* not equal *)
                   | LT (* less than *)
                   | GT (* greater than *)
                   | LE (* less or equal *)
                   | GE (* greater or equal *)
                   | ULT (* Unsigned less than *)
                   | ULE (* Unsigned less or equal *)
                   | UGT (* Unsigned greater than *)
                   | UGE (* Unsigned greater or equal *)

    val notRel : relop -> relop
    val commute: relop -> relop
end

structure Tree : TREE = struct
type label=Temp.label
type size = int

datatype stm = SEQ of stm * stm
             | LABEL of label
             | JUMP of exp * label list
             | CJUMP of relop * exp * exp * label * label
             | MOVE of exp * exp
             | EXP of exp

     and exp = BINOP of binop * exp * exp
             | MEM of exp
             | TEMP of Temp.temp
             | ESEQ of stm * exp
             | NAME of label
             | CONST of int
             | CALL of exp * exp list

     and binop = PLUS | MINUS | MUL | DIV
                 | AND | OR | LSHIFT | RSHIFT | ARSHIFT | XOR

     and relop = EQ | NE | LT | GT | LE | GE
                 | ULT | ULE | UGT | UGE

fun notRel EQ = NE
  | notRel NE = EQ
  | notRel LT = GE
  | notRel GT = LE
  | notRel LE = GT
  | notRel GE = LT
  | notRel ULT = UGE
  | notRel ULE = UGT
  | notRel UGT = ULE
  | notRel UGE = ULT

fun commute EQ = EQ
  | commute NE = NE
  | commute LT = GT
  | commute GE = LE
  | commute GT = LT
  | commute LE = GE
  | commute ULT = UGT
  | commute ULE = UGE
  | commute UGT = ULT
  | commute UGE = ULE

end
