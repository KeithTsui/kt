structure AST = struct
open Symbol
type pos = int

datatype var = SimpleVar of symbol * pos             (* x *)
             | FieldVar of var * symbol * pos        (* x.a *)
             | SubscriptVar of var * exp * pos       (* x[a+2] *)

     and exp = VarExp of var                         (* x *)
             | NilExp                                (* nil *)
             | IntExp of int                         (* 1 *)
             | StringExp of string * pos             (* "abc" *)
             | CallExp of {func: symbol, args: exp list, pos: pos}  (* f(args)  *)
             | OpExp of {left: exp, oper: oper, right: exp, pos: pos} (* exp1 op exp2 *)
             | RecordExp of {fields: (symbol * exp * pos) list,     (* {a:1, b:2, c:"name"}  *)
                             typ: symbol, pos: pos}
             | SeqExp of (exp * pos) list  (* (exp1; exp2; exp3) *)
             | AssignExp of {var: var, exp: exp, pos: pos}  (* x := 1 *)
             | IfExp of {test: exp, then': exp, else': exp option, pos: pos} (* if x then y else z *)
             | WhileExp of {test: exp, body: exp, pos: pos}
             | ForExp of {var: symbol, escape: bool ref,
                          lo: exp, hi: exp, body: exp, pos: pos}
             | BreakExp of pos
             | LetExp of {decs: dec list, body: (exp * pos) list, pos: pos}
             | ArrayExp of {typ: symbol, size: exp, init: exp, pos: pos}

     and dec = FunctionDec of fundec
             | VarDec of vardec_rt
             | TypeDec of typedec_rt

     and oper = PlusOp | MinusOp | TimesOp | DivideOp
                | EqOp | NeqOp | LtOp | LeOp | GtOp | GeOp
                | AndOp | OrOp

     and ty = NameTy of symbol * pos
            | RecordTy of field list
            | ArrayTy of symbol * pos


withtype field = {name: symbol, escape: bool ref,
                  typ: symbol, pos: pos}



     and fundec = {name: symbol,
                   params: field list,
                   result: (symbol * pos) option,
                   body: exp,
                   pos: pos}

     and vardec_rt = {name: symbol,
                      escape: bool ref,
                      typ: (symbol * pos) option,
                      init: exp,
                      pos: pos}

     and typedec_rt = {name: symbol, ty: ty, pos: pos}



fun getSymbolFromVar(SimpleVar(s, p)) = s
  | getSymbolFromVar(FieldVar(v, s, p)) = getSymbolFromVar v
  | getSymbolFromVar(SubscriptVar(v, e, p)) = getSymbolFromVar v

end
