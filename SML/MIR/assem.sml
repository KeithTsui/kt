(* like MIR in llvm after ISel *)
structure Assem = struct

type reg = string
type temp = Temp.temp
type label = Temp.label


datatype instr =
         (* An operation *)
         (* The string of an instr may refer
          * to source registers ’s0, ’s1, ... ’s(k – 1),
          * and destination registers ’d0, ’d1, etc. *)
         OPER of {assem: string, (* instruction name in an ISA *)
                  dst: temp list, (* destination register *)
                  src: temp list, (* srouce register *)
                  jump: label list option} (* location of next instruction *)
         (* A lable is a point in a program to which jumps may go *)
         | LABEL of {assem: string, (* show how the label look in an assembly *)
                     lab: Temp.label} (* which label-symbol was used *)
         (* like an operation but must perform data transfer *)
         | MOVE of {assem: string,
                    dst: temp,
                    src: temp}

fun show (OPER{assem, dst, src, jump}) =
    let val regStringify = fn regs => (foldl (fn (r, s) => s ^ Temp.makestring r ) "" regs)
    in
        print("assem: " ^ assem
              ^ " dst: " ^ regStringify dst
              ^ " src: " ^ regStringify src
              ^ "\n" )
    end
  | show (LABEL{assem, lab}) =
    print("assem: " ^ assem
          ^ "\n")
  | show (MOVE{assem, dst, src}) =
    print("assem: " ^ assem
          ^ " dst: " ^ Temp.makestring dst
          ^ " src: " ^ Temp.makestring src
          ^ "\n" )

(* Calling format(m)(i) formats an assembly instruction as string.
 * m is a function that shows the register assignment of every temp *)
fun format saytemp =
    let fun speak(assem,dst,src,jump) =
            let val saylab = Symbol.name
                fun f(#"`":: #"s":: i::rest) =
                    (explode(saytemp(List.nth(src, ord i - ord #"0"))) @ f rest)
                  | f( #"`":: #"d":: i:: rest) =
                    (explode(saytemp(List.nth(dst, ord i - ord #"0"))) @ f rest)
                  | f( #"`":: #"j":: i:: rest) =
                    (explode(saylab(List.nth(jump, ord i - ord #"0"))) @ f rest)
                  | f( #"`":: #"`":: rest) = #"`" :: f rest
                  | f( #"`":: _ :: rest) = ErrorMsg.impossible "bad Assem format"
                  | f(c :: rest) = (c :: f rest)
                  | f nil = nil
            in implode(f(explode assem))
            end
    in fn OPER{assem,dst,src,jump=NONE} => speak(assem,dst,src,nil)
     | OPER{assem,dst,src,jump=SOME j} => speak(assem,dst,src,j)
     | LABEL{assem,...} => assem
     | MOVE{assem,dst,src} => speak(assem,[dst],[src],nil)
    end
end
