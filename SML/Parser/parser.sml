structure Parse = struct
structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
structure Lex = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
structure TigerP = Join(structure ParserData = TigerLrVals.ParserData
                        structure Lex=Lex
                        structure LrParser = LrParser)

fun parse filename =
    let val _ = (ErrorMsg.reset(); ErrorMsg.fileName := filename)
        val file = TextIO.openIn filename
        fun get _ = TextIO.input file
        fun parseerror(s,p1,p2) = ErrorMsg.error p1 s
        val lexer = LrParser.Stream.streamify (Lex.makeLexer get)
        val (result, _) = TigerP.parse(0, lexer, parseerror, ())
    in
        (
          TextIO.closeIn file;
          print(filename ^ " AST :\n");
          ASTPrinter.print(TextIO.stdOut, result);
          print("\n");
          result
        )
    end
    handle LrParser.ParseError
           => raise LrParser.ParseError

end (* structure Parse *)
