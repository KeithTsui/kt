(* 1. Calling Conventions
 * 2. Register set
 * 3. Stack frame allocation for a variable
 * 4. Remember frame contents
 *)

signature FRAME = sig
    (* the type frame holds information about formal parameters and local variables allocated in this frame*)
    (* { name: TP.label,
     *   formals: access list,
     *   locals: int ref, // top of frame stack
     *   instrs: T.stm list }  *)
    type frame

    (* The access type describes formal parameters and locals that may be in the frame or in registers *)
    (* InFrame of int | InReg of TP.temp *)
    type access

    datatype frag = PROC of {body: Tree.stm, frame: frame}
                  | STRING of Temp.label * string

    (* formals:bool list a list of escaped! attributes for parameters *)
    val newFrame : {name: Temp.label,
                    formals: bool list} -> frame

    (* frame getter function *)
    val name: frame -> Temp.label
    val formals: frame -> access list

    (* allocate a local variable in a frame, which is escaped or not *)
    val allocLocal : frame -> bool -> access

    (* generate assembly for a string *)
    val string : Tree.label * string -> string

    val FP: Temp.temp (* Frame pointer *)
    val SP: Temp.temp (* Stack pointer *)
    val RV: Temp.temp (* return value *)
    val RA: Temp.temp (* return address *)

    (* wordsize is the quotion between the size of register to the size of addressable unit in memory *)
    (* e.g. register size is 32 bits, and memory unit is 8 bits, then word size would be 4  *)
    (* in other words, the size of register in terms of addressable units in memory *)
    val wordSize: int

    (* is used by Translate to turn a Frame.access into the Tree expression *)
    val exp: access -> Tree.exp -> Tree.exp

    type register = string
    val registers : register list

    (*argregs a list of μ registers in which to pass outgoing arguments
     *(including the static link);
     *)
    val argregs : Temp.temp list

    (*calleesaves a list of μ registers that the called procedure
     *(callee) must preserve unchanged (or save and restore);*)
    val callersaves : Temp.temp list

    (* callersaves a list of μ registers that the callee may trash.*)
    val calleesaves : Temp.temp list

    (* register ID to register machine name *)
    (* e.g. 100 -> "$a0" *)
    val tempMap : register Temp.Table.table

    (* get register name from register ID *)
    (* e.g. 100 -> "$a0" *)
    (* if not existed, make a name for it *)
    (* e.g. 100 -> "t100" *)
    val temp_name : Temp.temp -> string

    (* The procedure entry and exit sequences,
     * will be added as special "glue" around function body for each target machine
     * i.e. prologue and epilogue of a function *)

    (* for each incoming register parameter, move it to the place
     * from which it is seem from within the function. This could be
     * a frame location (for escaping parameters) or a fresh temporary.
     * and calleesaves
     *)
    val procEntryExit1 : frame * Tree.stm -> Tree.stm

    (* This function appends a “sink” instruction
     * to the function body to tell the register allocator
     * that certain registers are live at procedure exit. *)
    val procEntryExit2 : frame * Assem.instr list -> Assem.instr list

    (* prologue and epilogue *)
    val procEntryExit3 : frame * Assem.instr list ->
                         {prolog:string,body:Assem.instr list,epilog:string}



    val externalCall : string * Tree.exp list -> Tree.exp

end
