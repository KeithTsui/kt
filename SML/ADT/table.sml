functor IntMapTable (
    type key
    val hash: key -> int) : TABLE = struct

type key = key

(*https://web.mit.edu/~firebird/arch/sun4x_59/bin/sml/smlnj-lib/Util/int-binary-map.sml*)
type 'v table = 'v IntBinaryMap.map

val empty = IntBinaryMap.empty

fun enter (t, k, v) = IntBinaryMap.insert(t, hash k, v)

fun look (t, k) = IntBinaryMap.find(t, hash k)

end
