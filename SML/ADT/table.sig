signature TABLE =
sig
    type key
    type 'value table
    val empty : 'value table
    val enter : 'value table * key * 'value -> 'value table
    val look  : 'value table * key -> 'value option
end
