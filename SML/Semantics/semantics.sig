signature SEMANT =
sig
    structure Translate: TRANSLATE
    val transProg : AST.exp -> Translate.frag list
end
