structure Types = struct

(*use to compare address of value*)
(*
val x: unique = ref ();
val y: unique = ref ();
 x =  y; // false
!x = !y; // true
*)
type unique = unit ref

datatype ty =
         RECORD of (Symbol.symbol * ty) list * unique
         | NIL
         | UNIT
         | INT
         | STRING
         | ARRAY of ty * unique
         | NAME of Symbol.symbol * ty option ref (* for mutually recursive types *)

end (* structure Types *)
