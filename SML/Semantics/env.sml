(* An Environment is a symbol table
 * There two environment:
 * tenv for type environment
 * venv for variable environment
 * In addition, there two base environments as well,
 * base_tenv for predefined types in type environment,
 * base-venv for predefined functions in variable environment.
 *)

functor EnvFunctor (Translate: TRANSLATE) : ENV  = struct


structure Translate  = Translate
structure S = Symbol
structure T = Types

type ty = Types.ty

datatype enventry
  (* An access is infos for accessing the variable
   * which activation record and (frame offset if in frame or which register in register)
   *)
  = VarEntry of {access: Translate.access, ty: ty}
  (* which activation record, label(address) of the function,
   * function types, input/output
   *)
  | FunEntry of {level: Translate.level,
                 label: Temp.label,
                 formals: ty list,
                 result: ty}

(* value semantices *)
val base_tenv
    = S.enter(S.enter(
                   S.empty,
                   S.symbol("int"),
                   T.INT),
              S.symbol("string"),
              T.STRING)

(* function name and its function type *)
type fun_info = string * ty list * ty
val base_funs : fun_info list =
    [("print",[T.STRING],T.UNIT),
     ("printi",[T.INT],T.UNIT),
     ("flush",[],T.UNIT),
     ("getchar",[],T.STRING),
     ("ord",[T.STRING],T.INT),
     ("chr",[T.INT],T.STRING),
     ("size",[T.STRING],T.INT),
     ("substring",[T.STRING,T.INT,T.INT],T.INT),
     ("concat",[T.STRING,T.STRING],T.STRING),
     ("not",[T.INT],T.INT),
     ("exit",[T.INT],T.UNIT)]

(* value semantics *)
val base_venv =
    List.foldr
        (fn ((name,formals,result),env) =>
            let val label = Temp.namedlabel name in
                S.enter (env,S.symbol(name),
                         FunEntry{level=Translate.newLevel
                                            {parent=Translate.outermost,
                                             name=label,
                                             formals=map (fn _ => false) formals},
                                  label=label,
                                  formals=formals,
                                  result=result})
            end)
        S.empty base_funs

end
