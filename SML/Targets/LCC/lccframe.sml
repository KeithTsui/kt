structure LCCFrame: FRAME = struct

structure T = Tree
structure A = Assem
structure S = Symbol
structure TP = Temp

type register = string
datatype access = InFrame of int | InReg of TP.temp

type frame
     = { name: TP.label,
         formals: access list,
         locals: int ref, (* top of frame stack *)
         instrs: T.stm list (*IR instructions for moving incoming InReg args into Stack
                             * Nil for LCC, because all arguments are passed in stack *)
       }


(* code and data fragment of a program, In IR form *)
datatype frag
  = PROC of {body:T.stm, frame:frame}
  | STRING of TP.label * string

exception RegisterNotExisted of TP.temp

(* the size of register is the same as addressable unit in memory *)
(* in other words, the memory is word addressable *)
val wordSize = 1

(* there are 8 general purpose register *)

(* expression evaluation and results of a functioin *)
val r1 = TP.newtemp()
val r2 = TP.newtemp()
val r3 = TP.newtemp()
val r4 = TP.newtemp()

val FP = TP.newtemp() (* r5 frame pointer *)
val SP = TP.newtemp() (* r6 stack pointer *)
val RA = TP.newtemp() (* r7 Link register *)
val RV = TP.newtemp() (* r0 return Value *)

val specialargs = [FP, SP, RA, RV]

(* all arguments passing on stack *)
val argregs = []: TP.temp list

val calleesaves = []: TP.temp list

val callersaves = [r1, r2, r3, r4]

val reglist =
    [("r0",RV),("r1",r1),("r2",r2),("r3",r3),
     ("r4",r4),("fp",FP),("sp",SP),("lr",RA)]


(* register ID to register machine name *)
(* e.g. 100 -> "$a0" *)
val tempMap = foldl (fn ((k,v),tb) => TP.Table.enter(tb,v,k))
                    TP.Table.empty
                    reglist

(* get register name from register ID *)
(* e.g. 100 -> "$a0" *)
(* if not existed, make a name for it *)
(* e.g. 100 -> "t100" *)
fun temp_name t =
    case TP.Table.look(tempMap,t) of
        SOME(r) => r
      | NONE => Temp.makestring t

(* a list of all register machine name, which can be used for coloring *)
val registers = map (fn (x) => case TP.Table.look(tempMap,x) of
                                   SOME(r) => r
                                 | NONE => raise RegisterNotExisted(x))
                    (argregs @ calleesaves @ callersaves @ specialargs)

(* generate assembly for a string *)
fun string (label, str) : string =
    S.name label ^ ": .string \"" ^ str ^ "\"\n"

(* MIR for locals in frame or in register *)
fun exp (InFrame(k))= (fn (temp) => T.MEM(T.BINOP(T.PLUS,temp,T.CONST k)))
  | exp (InReg(temp)) = (fn (_) => T.TEMP temp)

(* make a new frame object, which also includes "view shift" instructions.
 * Note: right now we only pass parameters through register a0-a3. For
 * functions with more than 4 paramters, we just give up.. *)
fun newFrame {name: TP.label, formals: bool list} =
    let
        val n = List.length formals

        fun iter(nil,_) = nil
          | iter(_::rest, offset) = InFrame(offset)::iter(rest, offset + wordSize)

        val accs : access list = iter(formals, wordSize) (* save old FP at offset 0 *)

        fun view_shift(acc,r) = T.MOVE(exp acc (T.TEMP FP), T.TEMP r)

        val shift_instrs = ListPair.map view_shift (accs, argregs)
    in
        if n <= List.length argregs then
            {name=name,formals=accs,locals=ref 0,instrs=shift_instrs}
        else
            (*???*)
            {name=name,formals=accs,locals=ref 0,instrs=shift_instrs}
    end

fun name ({name,...}: frame) = name
fun formals ({formals,...}: frame): access list = formals

(* allocate a local variable either on frame or in register *)
fun allocLocal ({locals,...}: frame) escape =
    if (escape) then
        let val ret = InFrame((!locals+1)*(~wordSize)) in
            locals := !locals + 1; ret end
    else InReg(TP.newtemp())


fun externalCall (s,args) = T.CALL(T.NAME(TP.namedlabel s), args)


fun seq nil = T.EXP(T.CONST 0)
  | seq [st] = st
  | seq (st :: rest) = T.SEQ(st,seq(rest))

(* for each incoming register parameter, move it to the place
 * from which it is seem from within the function. This could be
 * a frame location (for escaping parameters) or a fresh temporary.
 * and calleesaves
 *)
fun procEntryExit1 (frame,body) : T.stm =
    let
        val pairs =  map (fn r => (allocLocal frame false,r)) (calleesaves)
        val saves = map (fn (a,r) => T.MOVE(exp a (T.TEMP FP),T.TEMP r)) pairs
        val restores = map (fn (a,r) => T.MOVE(T.TEMP r,exp a (T.TEMP FP)))
                           (List.rev pairs)
    in seq(saves @ [body] @ restores) end

val inArgsMove = procEntryExit1

(* This function appends a “sink” instruction
 * to the function body to tell the register allocator
 * that certain registers are live at procedure exit. *)
fun procEntryExit2 (frame,body) =
    body @
    [A.OPER{assem="",
            src=[RA, SP, FP, RV]@calleesaves,
            dst=[],
            jump=SOME[]}]

val procLiveOutsMark = procEntryExit2

(* prologue and epilogue *)
fun procEntryExit3 ({name,formals,locals,instrs},body) =
    let val offset = (!locals + (List.length argregs))*wordSize in
        {prolog = S.name name ^ ":\n"
                  ^ "\t push \t lr \n"  (* save return address from LR *)
                  ^ "\t push \t fp \n"  (* save caller's frame pointer *)
                  ^ "\t mov \t fp, sp \n" (* make callee's frame pointer *)
                  ^ "",
         body = body,
         epilog = "\t mov \t sp, fp \n" (* clean callee's stack *)
                  ^  "\t pop \t fp \n" (* restore caller's FP *)
                  ^  "\t pop \t lr \n" (* restore return address in LR *)
                  ^ "\t ret \n" (* jump back to LR *)
        }
    end

val proEpilogue = procEntryExit3
end
