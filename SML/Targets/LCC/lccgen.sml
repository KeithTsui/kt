structure LCCGen : CODEGEN = struct

structure Frame : FRAME = LCCFrame
structure A = Assem
structure T = Tree

exception TooManyArgs of string

fun codegen (frame) (stm:Tree.stm) : Assem.instr list =
    let val ilist = ref (nil: A.instr list)
        fun emit x = ilist := x :: !ilist
        fun result(gen) = let val t = Temp.newtemp() in gen t; t end

        fun int2str n =
            if n >= 0 then Int.toString(n) else ("-" ^ Int.toString(~n))

        (* calling a function will trash a particular set of registers:
         *   - argregs: they are defined to pass parameters;
         *   - callersaves: they may be redefined inside the call;
         *   - RV: it will be overwritten for function return.
         *)
        val calldefs = [Frame.RV]

        fun munchStm (T.SEQ(a,b)) = (munchStm(a); munchStm(b))

          | munchStm (T.LABEL lab) =
            emit(A.LABEL{assem=Symbol.name(lab) ^ ":", lab=lab})

          (* data movement instructions *)

          (* 1, store to memory (sw) *)

          (* Mem[e1 + i] <- e2 => str e2, e1, i *)
          | munchStm (T.MOVE(T.MEM(T.BINOP(T.PLUS, e1, T.CONST i)), e2)) =
            emit(A.OPER{assem="str `s0, `s1, " ^ int2str i ,
                        src=[munchExp e2, munchExp e1],
                        dst=[],
                        jump=NONE})

          (* Mem[i + e1] <- e2 => str e2, e1, i*)
          | munchStm (T.MOVE(T.MEM(T.BINOP(T.PLUS, T.CONST i, e1)), e2)) =
            emit(A.OPER{assem="str `s0, `s1, " ^ int2str i ,
                        src=[munchExp e2, munchExp e1],
                        dst=[],
                        jump=NONE})

          (* Mem[e1 - i] <- e2 = str e2, e1, -i *)
          | munchStm (T.MOVE(T.MEM(T.BINOP(T.MINUS, e1, T.CONST i)), e2)) =
            emit(A.OPER{assem="str `s0, `s1, " ^ int2str (~i) ,
                        src=[munchExp e2, munchExp e1],
                        dst=[],
                        jump=NONE})

          (* Mem[i - e1] <- e2 = str e2, e1, -i *)
          | munchStm (T.MOVE(T.MEM(T.BINOP(T.MINUS, T.CONST i, e1)), e2)) =
            emit(A.OPER{assem="str `s0, `s1, " ^ int2str i ,
                        src=[munchExp e2, munchExp(T.BINOP(T.MINUS, T.CONST 0, e1))],
                        dst=[],
                        jump=NONE})

          (* Mem[e1] <- e2 *)
          | munchStm (T.MOVE(T.MEM(e1), e2)) =
            emit(A.OPER{assem="str `s0, `s1, 0",
                        src=[munchExp e2, munchExp e1],
                        dst=[],
                        jump=NONE})

          (* 2, load to register (lw) *)
          (* ri <- n *)
          | munchStm (T.MOVE((T.TEMP i, T.CONST n))) =
            emit(A.OPER{assem="mvi `d0, " ^ int2str n,
                        src=[],
                        dst=[i],
                        jump=NONE})

          (*ri <- Mem[e1 + n] *)
          | munchStm (T.MOVE(T.TEMP i,
                             T.MEM(T.BINOP(T.PLUS, e1, T.CONST n)))) =
            emit(A.OPER{assem="ldr `d0, `s0, " ^ int2str n,
                        src=[munchExp e1],
                        dst=[i],
                        jump=NONE})

          (* ri <- Mem[n + e1] *)
          | munchStm (T.MOVE(T.TEMP i,
                             T.MEM(T.BINOP(T.PLUS, T.CONST n, e1)))) =
            emit(A.OPER{assem="ldr `d0, `s0, " ^ int2str n,
                        src=[munchExp e1],
                        dst=[i],
                        jump=NONE})

          (* ri <- Mem[e1 - n] *)
          | munchStm (T.MOVE(T.TEMP i,
                             T.MEM(T.BINOP(T.MINUS, e1, T.CONST n)))) =
            emit(A.OPER{assem="ldr `d0, `s0, " ^ int2str (~n),
                        src=[munchExp e1],
                        dst=[i],
                        jump=NONE})

          (* ri <- Mem[n - e1] *)
          | munchStm (T.MOVE(T.TEMP i,
                             T.MEM(T.BINOP(T.MINUS, T.CONST n, e1)))) =
            emit(A.OPER{assem="ldr `d0, `s0, " ^ int2str i ,
                        src=[ munchExp(T.BINOP(T.MINUS, T.CONST 0, e1))],
                        dst=[i],
                        jump=NONE})

          (* 3, move from register to register *)
          (* ri <- e2 *)
          | munchStm (T.MOVE((T.TEMP i, e2))) =
            emit(A.MOVE{assem="mvr `d0, `s0",
                        src=munchExp e2,
                        dst=i})

          (* branching *)
          (* jump lab *)
          | munchStm (T.JUMP(T.NAME lab, _)) =
            emit(A.OPER{assem="br `j0",
                        src=[],
                        dst=[],
                        jump=SOME([lab])})

          (* jump e *)
          | munchStm (T.JUMP(e, labels)) =
            emit(A.OPER{assem="jmp `s0",
                        src=[munchExp e],
                        dst=[],
                        jump=SOME(labels)})

          (* when comparing with 0 *)
          (* e1 >= 0 ? l1:l2 *)
          | munchStm (T.CJUMP(T.GE, e1, T.CONST 0, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, 0\n brlt `j1 \n br `j0",
                        dst=[],
                        src=[munchExp e1],
                        jump=SOME [l1,l2]})

          (* e1 > 0 ? l1 : l2*)
          | munchStm (T.CJUMP(T.GT, e1, T.CONST 0, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, 0\n brgt `j0 \n br `j1",
                        dst=[],
                        src=[munchExp e1],
                        jump=SOME [l1,l2]})

          (* e1 <= 0 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.LE, e1, T.CONST 0, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, 0\n brgt `j1 \n br `j0",
                        dst=[],
                        src=[munchExp e1],
                        jump=SOME [l1,l2]})

          (* e1 < 0 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.LT, e1, T.CONST 0, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, 0\n brlt `j0 \n br `j1",
                        dst=[],
                        src=[munchExp e1],
                        jump=SOME [l1,l2]})

          (* e1 == 0 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.EQ, e1, T.CONST 0, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, 0\n brz `j0 \n br `j1",
                        dst=[],
                        src=[munchExp e1],
                        jump=SOME [l1,l2]})

          (* e1 != 0 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.NE, e1, T.CONST 0, l1, l2)) =
            emit(A.OPER{
                      assem="cmp `s0, 0\n brnz `j0 \n br `j1",
                      dst=[],
                      src=[munchExp e1],
                      jump=SOME [l1,l2]})

          (* more general cases *)

          (* e1 >= e2 ? l1 : l2  *)
          | munchStm (T.CJUMP(T.GE, e1, e2, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, `s1\n brlt `j1 \n br `j0",
                        dst=[],
                        src=[munchExp e1, munchExp e2],
                        jump=SOME [l1,l2]})

          (* e1 >= e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.UGE, e1, e2, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, `s1 \n brc `j1 \n br `j0",
                        dst=[],
                        src=[munchExp e1, munchExp e2],
                        jump=SOME [l1,l2]})

          (* e1 > e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.GT, e1, e2, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, `s1 \n brgt `j0 \n br `j1",
                        dst=[],
                        src=[munchExp e1, munchExp e2],
                        jump=SOME [l1,l2]})

          (* e1 > e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.UGT, e1, e2, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, `s1 \n brc `j1 \n brz `j1 \n br `j0",
                        dst=[],
                        src=[munchExp e1, munchExp e2],
                        jump=SOME [l1,l2]})

          (* e1 < e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.LT, e1, e2, l1, l2)) =
            emit(A.OPER{assem="cmp `s0, `s1 \n brlt `j0 \n br `j1",
                        dst=[],
                        src=[munchExp e1, munchExp e2],
                        jump=SOME [l1,l2]})

          (* e1 < e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.ULT, e1, e2, l1, l2)) =
            emit(A.OPER{
                      assem="cmp `s0, `s1 \n brc `j0 \n br `j1",
                      dst=[],
                      src=[munchExp e1, munchExp e2],
                      jump=SOME [l1,l2]})

          (* e1 <= e2 ? l1 : l2*)
          | munchStm (T.CJUMP(T.LE, e1, e2, l1, l2)) =
            emit(A.OPER{
                      assem="cmp `s0, `s1 \n brgt `j1 \n br `j0",
                      dst=[],
                      src=[munchExp e1, munchExp e2],
                      jump=SOME [l1,l2]})

          (* e1 <= e2 ? l1 : l2*)
          | munchStm (T.CJUMP(T.ULE, e1, e2, l1, l2)) =
            emit(A.OPER{
                      assem="cmp `s0, `s1 \n brc `j0 \n brz `j0 \n br `j1",
                      dst=[],
                      src=[munchExp e1, munchExp e2],
                      jump=SOME [l1,l2]})

          (* e1 == e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.EQ, e1, e2, l1, l2)) =
            emit(A.OPER{
                      assem="cmp `s0, `s1\n brz `j0 \n br `j1",
                      dst=[],
                      src=[munchExp e1, munchExp e2],
                      jump=SOME [l1,l2]})

          (* e1 != e2 ? l1 : l2 *)
          | munchStm (T.CJUMP(T.NE, e1, e2, l1, l2)) =
            emit(A.OPER{
                      assem="cmp `s0, `s1 \n brnz `j0 \n br `j1",
                      dst=[],
                      src=[munchExp e1, munchExp e2],
                      jump=SOME [l1,l2]})

          | munchStm (T.EXP e) = (munchExp e; ())

        (* memory ops *)

        (* Mem[i]*)
        and munchExp (T.MEM(T.CONST i)) =
            result(fn r => emit(A.OPER{
                                     assem="ldr `d0, `s0, 0",
                                     src=[munchExp(T.CONST i)],
                                     dst=[r],
                                     jump=NONE}))

          (* Mem[e1 + i] *)
          | munchExp (T.MEM(T.BINOP(T.PLUS, e1, T.CONST i))) =
            result(fn r => emit(A.OPER{
                                     assem="ldr `d0, `s0, " ^ int2str i ,
                                     src=[munchExp e1],dst=[r],jump=NONE}))

          (* Mem[i + e2] *)
          | munchExp (T.MEM(T.BINOP(T.PLUS, T.CONST i, e2))) =
            result(fn r => emit(A.OPER{
                                     assem="ldr `d0, `s0, " ^ int2str i ,
                                     src=[munchExp e2],dst=[r],jump=NONE}))

          (* Mem[e1 - i] *)
          | munchExp (T.MEM(T.BINOP(T.MINUS, e1, T.CONST i))) =
            result(fn r => emit(A.OPER{
                                     assem="ldr `d0, `s0, " ^ int2str (~i) ,
                                     src=[munchExp e1],dst=[r],jump=NONE}))
          (* Mem[i - e2] *)
          | munchExp (T.MEM(T.BINOP(T.MINUS, T.CONST i, e2))) =
            result(fn r => emit(A.OPER{
                                     assem="ldr `d0, `s0, " ^ int2str (~i) ,
                                     src=[munchExp e2],dst=[r],jump=NONE}))
          (* Mem[e1]  *)
          | munchExp (T.MEM(e1)) =
            result(fn r => emit(A.OPER{
                                     assem="ldr `d0, `s0, 0" ,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* binary operations *)

          (* 1, add/sub immediate *)

          (* r <- e1 + i*)
          | munchExp (T.BINOP(T.PLUS, e1, T.CONST i)) =
            result(fn r => emit(A.OPER{
                                     assem="add `d0, `s0, " ^ int2str i,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- i + e1 *)
          | munchExp (T.BINOP (T.PLUS, T.CONST i, e1)) =
            result(fn r => emit(A.OPER{
                                     assem="add `d0, `s0, " ^ int2str i,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- e1 + e2 *)
          | munchExp (T.BINOP(T.PLUS, e1, e2)) =
            result(fn r => emit(A.OPER{
                                     assem="add `d0, `s0, `s1",
                                     src=[munchExp e1,munchExp e2],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- e1 - i *)
          | munchExp (T.BINOP(T.MINUS, e1, T.CONST i)) =
            result(fn r => emit(A.OPER{
                                     assem="sub `d0, `s0, " ^ int2str (i),
                                     src=[munchExp e1],dst=[r],jump=NONE}))

          (* div *)
          (* r <- e1 / e2 *)
          | munchExp (T.BINOP(T.DIV, e1, e2)) =
            result(fn r => emit(A.OPER{
                                     assem="div `d0, `s0, `s1",
                                     src=[munchExp e1,munchExp e2],
                                     dst=[r],jump=NONE}))

          (* mul *)
          (* r <- e1 * e2 *)
          | munchExp (T.BINOP(T.MUL, e1, e2)) =
            result(fn r => emit(A.OPER{
                                     assem="mul `d0, `s0, `s1",
                                     src=[munchExp e1,munchExp e2],
                                     dst=[r],jump=NONE}))

          (* r <- e1 - e2 *)
          | munchExp (T.BINOP(T.MINUS, e1, e2)) =
            result(fn r => emit(A.OPER{
                                     assem="sub `d0, `s0, `s1",
                                     src=[munchExp e1, munchExp e2],
                                     dst=[r],jump=NONE}))

          (* and *)

          (* r <- e1 & n *)
          | munchExp (T.BINOP (T.AND, e1, T.CONST n)) =
            result(fn r => emit(A.OPER{
                                     assem="and `d0, `s0, " ^ int2str n,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- n & e1 *)
          | munchExp (T.BINOP (T.AND, T.CONST n, e1)) =
            result(fn r => emit(A.OPER{
                                     assem="and `d0, `s0, " ^ int2str n,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- e1 & e2 *)
          | munchExp (T.BINOP (T.AND, e1, e2)) =
            result(fn r => emit(A.OPER{
                                     assem="and `d0, `s0, `s1",
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* or *)
          (* r <- e1 | n *)
          | munchExp (T.BINOP (T.OR, e1, T.CONST n)) =
            result(fn r => emit(A.OPER{
                                     assem="or `d0, `s0, " ^ int2str n,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- n | e1 *)
          | munchExp (T.BINOP (T.OR, T.CONST n, e1)) =
            result(fn r => emit(A.OPER{
                                     assem="or `d0, `s0, " ^ int2str n,
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* r <- e1 | e2 *)
          | munchExp (T.BINOP (T.OR, e1, e2)) =
            result(fn r => emit(A.OPER{
                                     assem="or `d0, `s0, `s1",
                                     src=[munchExp e1],
                                     dst=[r],
                                     jump=NONE}))

          (* shift *)
          (* r <- e << n => sll e, n; e *)
          | munchExp (T.BINOP (T.LSHIFT, e, T.CONST n)) =
            result (fn r =>
                       let
                           val sr = munchExp e
                           val ct = munchExp (T.CONST n)
                       in

                           emit (A.OPER {
                                      assem="mvr `d0, `s0 \n sll `d0, `s1",
                                      src=[sr, ct],
                                      dst=[r],
                                      jump=NONE})
                       end)

          (* r <- e1 << e2*)
          | munchExp (T.BINOP (T.LSHIFT, e1, e2)) =
            result (fn r =>
                       let
                           val sr = munchExp e1
                           val ct = munchExp e2
                       in

                           emit (A.OPER {
                                      assem="mvr `d0, `s0 \n sll `d0, `s1",
                                      src=[sr, ct],
                                      dst=[r],
                                      jump=NONE})
                       end)

          (* r <- e >> n *)
          | munchExp (T.BINOP (T.RSHIFT, e, T.CONST n)) =
            result (fn r =>
                       let
                           val sr = munchExp e
                           val ct = munchExp (T.CONST n)
                       in

                           emit (A.OPER {
                                      assem="mvr `d0, `s0 \n srl `d0, `s1",
                                      src=[sr, ct],
                                      dst=[r],
                                      jump=NONE})
                       end)

          (* r <- e1 >> e2*)
          | munchExp (T.BINOP (T.RSHIFT, e1, e2)) =
            result (fn r =>
                       let
                           val sr = munchExp e1
                           val ct = munchExp e2
                       in

                           emit (A.OPER {
                                      assem="mvr `d0, `s0 \n srl `d0, `s1",
                                      src=[sr, ct],
                                      dst=[r],
                                      jump=NONE})
                       end)

          | munchExp (T.BINOP (T.ARSHIFT, e, T.CONST n)) =
            result (fn r =>
                       let
                           val sr = munchExp e
                           val ct = munchExp (T.CONST n)
                       in

                           emit (A.OPER {
                                      assem="mvr `d0, `s0 \n sra `d0, `s1",
                                      src=[sr, ct],
                                      dst=[r],
                                      jump=NONE})
                       end)

          | munchExp (T.BINOP (T.ARSHIFT, e1, e2)) =
            result (fn r =>
                       let
                           val sr = munchExp e1
                           val ct = munchExp e2
                       in

                           emit (A.OPER {
                                      assem="mvr `d0, `s0 \n sra `d0, `s1",
                                      src=[sr, ct],
                                      dst=[r],
                                      jump=NONE})
                       end)

          (* Immediate i  *)
          | munchExp (T.CONST i) =
            result(fn r => emit(A.OPER{
                                     assem="mvi `d0, " ^ int2str i,
                                     src=[],
                                     dst=[r],
                                     jump=NONE}))

          (* Register  *)
          | munchExp (T.TEMP t) = t

          (* load effective address of a label *)
          | munchExp (T.NAME label) =
            result(fn r => emit(A.OPER{
                                     assem="lea `d0, " ^ Symbol.name label,
                                     src=[],
                                     dst=[r],
                                     jump=NONE}))

          (* A procedure call *)
          | munchExp (T.CALL(e,args)) =
            result(fn r =>
                      let
                          val regArgs = munchArgs(0, args)
                          val regFunc = munchExp(e)
                      in
                          emit(A.OPER{
                                    assem="jsrr `s0",
                                    src=regFunc::regArgs,
                                    dst=calldefs,
                                    jump=NONE})
                      end)

        (* generate code to move all arguments to their correct positions.
         * In LCC, arguments are passed in frame *)
        and munchArgs (n, args) = munchRevArgs (n, rev args)
        and munchRevArgs(_, nil) = nil
          | munchRevArgs(i, exp :: rest) =
            (emit(A.OPER{
                       assem="push `s0",
                       src=[munchExp(exp)],
                       dst=[],
                       jump=NONE}); munchRevArgs(i, rest))



    in (munchStm stm; rev(!ilist)) end (* let-in-end *)
end (* fun codegen *)
