//===-- Machine.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "Machine.h"
#include "LCC/LCC.h"
#include "LCC/Utilities/utilities.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Path.h"
#include <bitset>
#include <cassert>
#include <cstddef>
#include <ios>
#include <iostream>
#include <vector>

namespace LCC {

bool Machine::load(std::string program, uint16_t loadPoint) {
  if (program.empty())
    return false;
  auto inputFilename = program;
  auto inputFileExtension = llvm::sys::path::extension(inputFilename);
  if (inputFileExtension != LCC::executableExtension) {
    std::cerr << "not an executable file of extension "
              << inputFileExtension.str() << std::endl;
    exit(1);
  }
  auto inputOrError = llvm::MemoryBuffer::getFile(inputFilename);
  auto &inputBuffer = inputOrError.get();

  auto instructions = inputBuffer->getBuffer();

  uint16_t startOffset = 0;

  auto startDirectivePos = instructions.find('S');
  if (startDirectivePos != llvm::StringRef::npos) {
    auto startOffsetPos = instructions.data() + startDirectivePos + 1 + 1;
    const uint8_t *sop = reinterpret_cast<const uint8_t *>(startOffsetPos);
    uint16_t first = *sop;
    uint16_t second = *(sop + 1);
    startOffset = first | (second << 8);
  }

  auto beginPos = instructions.find('C');
  assert(beginPos != llvm::StringRef::npos && "cannot locate code segment");
  instructions = instructions.drop_front(beginPos + 1 + 1);
  auto programStart = instructions.data();
  auto programSize = instructions.size();
  auto programEnd = programStart + programSize;

  // copy program into machine memory
  for (uint16_t i = loadPoint; programStart != programEnd;
       programStart += 2, ++i) {
    const unsigned char first =
        *reinterpret_cast<const unsigned char *>(programStart);
    const unsigned char second =
        *reinterpret_cast<const unsigned char *>(programStart + 1);

    uint16_t f = static_cast<uint16_t>(first);
    uint16_t s = static_cast<uint16_t>(second);
    uint16_t res = (s << 8) | f;
    memory[i] = res;
  }

  pc = loadPoint + startOffset;

  return false;
}

void Machine::execute() {
  // extract operator code
  uint16_t opr = (ir & xtr12_15) >> 12;
  switch (opr) {
  case 0:
    execBranch();
    break;
  case 1:
    execAdd();
    break;
  case 2:
    execLd();
    break;
  case 3:
    execSt();
    break;
  case 4:
    execJump();
    break;
  case 5:
    execAnd();
    break;
  case 6:
    execLdr();
    break;
  case 7:
    execStr();
    break;
  case 8:
    execCmp();
    break;
  case 9:
    execNot();
    break;
  case 10:
    execArith();
    break;
  case 11:
    execSub();
    break;
  case 12:
    execRet();
    break;
  case 13:
    execMvi();
    break;
  case 14:
    execLea();
    break;
  case 15:
    execTrap();
    break;
  default:
    break;
  }
}

void Machine::execBranch() {
  uint16_t opr = curOpr();
  assert(opr == 0);
  uint16_t code = curReg0();
  uint16_t offset = offset9();
  switch (code) {
  case 0: // brz or bre
    if (z)
      signedSumInplace(pc, 16, offset, 8, false);
    // pc += offset9;
    break;
  case 1:
    if (!z)
      signedSumInplace(pc, 16, offset, 8, false);
    // pc += offset9;
    break;
  case 2:
    if (n)
      // pc += offset9;
      signedSumInplace(pc, 16, offset, 8, false);
    break;
  case 3:
    if (n == z)
      // pc += offset9;
      signedSumInplace(pc, 16, offset, 8, false);
    break;
  case 4:
    if (n != v)
      // pc += offset9;
      signedSumInplace(pc, 16, offset, 8, false);
    break;
  case 5:
    if (n == v)
      // pc += offset9;
      signedSumInplace(pc, 16, offset, 8, false);
    break;
  case 6:
    if (c == 1)
      // pc += offset9;
      signedSumInplace(pc, 16, offset, 8, false);
    break;
  case 7:
    // pc += offset9;
    signedSumInplace(pc, 16, offset, 8, false);
    break;
  default:
    assert(false);
  }
}

void Machine::execAdd() {
  uint16_t opr = curOpr();
  assert(opr == 1);
  uint16_t dr = curReg0();
  uint16_t sr = curReg1();
  uint16_t flag = flag6();
  int16_t res = 0, a = 0, b = 0;
  if (flag == 0) {
    uint16_t sr2 = ir & xtr0_2;
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&r[sr2]);
  } else {
    uint16_t imm5 = ir & xtr0_4;
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&imm5);
  }
  res = a + b;
  r[dr] = *reinterpret_cast<uint16_t *>(&res);
  n = res < 0 ? true : false;
  z = res == 0 ? true : false;
  v = (res < a || res < b) ? true : false;
  c = ((a > 0 && b > 0 && res < 0) || (a < 0 && b < 0 && res > 0)) ? true
                                                                   : false;
}

// ?
void Machine::execLd() {
  uint16_t opr = curOpr();
  assert(opr == 2);
  uint16_t dr = curReg0();
  uint16_t offset = offset9();
  r[dr] = memory[signedSum(pc, 15, offset, 8)];
}

// ?
void Machine::execSt() {
  uint16_t opr = curOpr();
  assert(opr == 3);
  uint16_t sr = curReg0();
  uint16_t offset = offset9();
  memory[signedSum(pc, 16, offset, 8)] = r[sr];
}

// ?
void Machine::execJump() {
  uint16_t opr = curOpr();
  assert(opr == 4);
  uint16_t flag = flag12();
  uint16_t offset = 0;
  uint16_t targetAddress = 0;
  if (flag == 0) {
    uint16_t baser = curReg1();
    uint16_t offset = offset6();
    targetAddress = signedSum(r[baser], 16, offset, 5);
  } else {
    targetAddress = offset11();
  }
  lr = pc;
  signedSumInplace(pc, 16, targetAddress, 10, false);
}

void Machine::execAnd() {
  uint16_t opr = curOpr();
  assert(opr == 5);
  uint16_t dr = curReg0();
  uint16_t sr = curReg1();
  uint16_t flag = flag6();
  int16_t res = 0, a = 0, b = 0;
  if (flag == 0) {
    uint16_t sr2 = curReg2();
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&r[sr2]);
  } else {
    uint16_t imm = imm5();
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&imm);
  }
  res = a & b;
  r[dr] = *reinterpret_cast<uint16_t *>(&res);
  n = res < 0 ? true : false;
  z = res == 0 ? true : false;
}

void Machine::execLdr() {
  uint16_t opr = curOpr();
  assert(opr == 6);
  uint16_t dr = curReg0();
  uint16_t baser = curReg1();
  uint16_t offset = offset6();
  r[dr] = memory[signedSum(r[baser], 16, offset, 5)];
}

void Machine::execStr() {
  uint16_t opr = curOpr();
  assert(opr == 7);
  uint16_t sr = curReg0();
  uint16_t baser = curReg1();
  uint16_t offset = offset6();
  memory[signedSum(r[baser], 16, offset, 5)] = r[sr];
}

void Machine::execCmp() {
  uint16_t opr = curOpr();
  assert(opr == 8);
  uint16_t sr = curReg1();
  uint16_t flag = flag6();
  int16_t res = 0, a = 0, b = 0;
  if (flag == 0) {
    uint16_t sr2 = curReg2();
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&r[sr2]);
  } else {
    uint16_t imm = imm5();
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&imm);
  }
  res = a - b;
  n = res < 0 ? true : false;
  z = res == 0 ? true : false;
  v = (res < a || res < b) ? true : false;
  c = ((a > 0 && b > 0 && res < 0) || (a < 0 && b < 0 && res > 0)) ? true
                                                                   : false;
}

void Machine::execNot() {
  uint16_t opr = curOpr();
  assert(opr == 9);
  uint16_t dr = curReg0();
  uint16_t sr = curReg1();
  uint16_t flag = flag6();
  int16_t res = *reinterpret_cast<int16_t *>(~r[sr]);
  r[dr] = ~r[sr];
  n = res < 0 ? true : false;
  z = res == 0 ? true : false;
}

void Machine::execArith() {
  uint16_t opr = curOpr();
  assert(opr == 10);
  uint16_t code = code2();
  uint16_t reg0 = curReg0();
  uint16_t reg1 = curReg1();
  switch (code) {
  case 0: // push
    memory[--sp] = r[reg0];
    break;
  case 1: // pop
    r[reg0] = memory[sp++];
    break;
  case 2:
    r[reg0] >>= r[reg1];
    break;
  case 3:
    r[reg0] >>= r[reg1];
    break;
  case 4:
    r[reg0] <<= r[reg1];
    break;
  case 5:
    r[reg0] <<= r[reg1];
    break;
  case 6:
    r[reg0] <<= r[reg1];
    break;
  case 7:
    r[reg0] *= r[reg1];
    break;
  case 8:
    r[reg0] /= r[reg1];
    break;
  case 9:
    r[reg0] %= r[reg1];
    break;
  case 10:
    r[reg0] |= r[reg1];
    break;
  case 11:
    r[reg0] ^= r[reg1];
    break;
  case 12:
    r[reg0] = r[reg1];
    break;
  default:
    break;
  }

  int16_t res = *reinterpret_cast<int16_t *>(&r[reg0]);
  n = res < 0;
  z = res == 0;

  // c?
}

void Machine::execSub() {
  uint16_t opr = curOpr();
  assert(opr == 11);
  uint16_t dr = curReg0();
  uint16_t sr = curReg1();
  uint16_t flag = flag6();
  int16_t res = 0, a = 0, b = 0;
  if (flag == 0) {
    uint16_t sr2 = curReg2();
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&r[sr2]);
  } else {
    uint16_t imm = imm5();
    a = *reinterpret_cast<int16_t *>(&r[sr]);
    b = *reinterpret_cast<int16_t *>(&imm);
  }
  b = -b;
  res = a + b;
  r[dr] = *reinterpret_cast<uint16_t *>(&res);
  n = res < 0 ? true : false;
  z = res == 0 ? true : false;
  v = (res < a || res < b) ? true : false;
  c = ((a > 0 && b > 0 && res < 0) || (a < 0 && b < 0 && res > 0)) ? true
                                                                   : false;
}

void Machine::execRet() {
  uint16_t opr = curOpr();
  assert(opr == 12);
  uint16_t code = curReg0();
  uint16_t baser = curReg1();
  uint16_t offset = offset6();
  switch (code) {
  case 0: // brz or bre
    if (z)
      pc = signedSum(r[baser], 16, offset, 5);
    // pc = r[baser] + offset;
    break;
  case 1:
    if (!z)

      // pc = r[baser] + offset;
      pc = signedSum(r[baser], 16, offset, 5);
    break;
  case 2:
    if (n)
      // pc = r[baser] + offset;
      pc = signedSum(r[baser], 16, offset, 5);
    break;
  case 3:
    if (n == z)
      // pc = r[baser] + offset;
      pc = signedSum(r[baser], 16, offset, 5);
    break;
  case 4:
    if (n != v)
      // pc = r[baser] + offset;
      pc = signedSum(r[baser], 16, offset, 5);
    break;
  case 5:
    if (n == v)
      // pc = r[baser] + offset;
      pc = signedSum(r[baser], 16, offset, 5);
    break;
  case 6:
    if (c == 1)
      // pc = r[baser] + offset;
      pc = signedSum(r[baser], 16, offset, 5);
    break;
  case 7:
    if (baser == 7)
      // pc = lr + offset;
      pc = signedSum(lr, 16, offset, 5);
    else
      signedSumInplace(pc, 16, offset, 5, false);
    break;
  default:
    assert(false);
  }
}

void Machine::execMvi() {
  uint16_t opr = curOpr();
  assert(opr == 13);
  uint16_t dr = curReg0();
  uint16_t offset = offset9();
  r[dr] = offset;
}

void Machine::execLea() {
  uint16_t opr = curOpr();
  assert(opr == 14);
  uint16_t dr = curReg0();
  uint16_t offset = offset9();
  r[dr] = signedSum(pc, 16, offset, 8);
}

void Machine::execTrap() {
  uint16_t opr = curOpr();
  assert(opr == 15);
  uint16_t code = code2();
  uint16_t reg = curReg0();
  assert(0 <= reg && reg <= 7);
  switch (code) {
  case 0: // halt
    halt = true;
    break;
  case 1: // nl
    std::cout << std::endl;
    break;
  case 2: // dout
    std::cout << std::dec << interpretAs<int16_t, uint16_t>(r[reg]);
    break;
  case 3: // udout
    std::cout << std::dec << r[reg];
    break;
  case 4: // hout
    std::cout << std::hex << r[reg];
    break;
  case 5: // aout
    std::cout << lowChar(r[reg]) << highChar(r[reg]);
    break;
  case 6: // sout
    std::cout << interpretAs<char *, uint16_t>(r[reg]);
    break;
  case 7: // din
  case 8: // hin
  case 9: // ain
    std::cin >> r[reg];
    break;
  case 10: {
    char buf[100] = {0};
    std::cin >> (buf + 1);
    char *index = buf;
    size_t size = {0};
    while (*(index++) != 0) {
      ++size;
    }
    memory[--sp] = 0;
    while (*(--index) != 0) {
      memory[--sp] = *index;
    }
    r[reg] = sp;
    break;
  }

  default:
    std::cout << r[reg];
    break;
  }
}

} // namespace LCC
