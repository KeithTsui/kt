//===-- Machine.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef MACHINE_H
#define MACHINE_H

#include <cstddef>
#include <cstdint>
#include <limits>
#include <string>


namespace LCC {

bool signOf(uint16_t a, size_t pos);
uint16_t signedMul(uint16_t a, size_t p, uint16_t b, size_t q);

// p, q specify the sign bit position, or the size of a, b
// which are less than or equal to 16.
// e.g pc += offset9, sum(pc, 15, offset9, 8)
uint16_t signedSum(uint16_t a, size_t p, uint16_t b, size_t q,
                   bool isSubtraction = false);
class ProgramLoader;
class Machine {
  uint16_t r[8];
  uint16_t &r0;
  uint16_t &r1;
  uint16_t &r2;
  uint16_t &r3;
  uint16_t &r4;
  uint16_t &r5;
  uint16_t &r6;
  uint16_t &r7;
  uint16_t pc;
  uint16_t ir;
  bool n, z, c, v;
  uint16_t &fp;
  uint16_t &sp;
  uint16_t &lr;

  uint16_t *memory;
  static constexpr uint16_t memorySize = std::numeric_limits<uint16_t>::max();

  Machine()
      : r{0}, r0{r[0]}, r1{r[1]}, r2{r[2]}, r3{r[3]}, r4{r[4]}, r5{r[5]},
        r6{r[6]}, r7{r[7]}, fp{r5}, sp{r6}, lr{r7}, pc{0}, ir{0}, n{false},
        z{false}, c{false}, v{false}, halt{false} {
    memory = new uint16_t[memorySize];
  }

  ~Machine() { delete[] memory; }

  bool halt;

public:
  Machine(Machine const &m) = delete;
  Machine(Machine const &&m) = delete;
  Machine &operator=(Machine const &m) = delete;
  Machine &operator=(Machine const &&m) = delete;

  static Machine &instance() {
    static Machine inst;
    return inst;
  }

  // a program loader is responsable for loading a program
  // into machine's memory started with load point
  // and set the PC to entry point.
  friend class ProgramLoader;

  bool load(std::string program, uint16_t loadPoint = 0);

  void run() {
    while (!halt) {
      fetch();
      execute();
    }
  }

private:
  // fetch the instruction the PC points to into IR
  void fetch() { ir = memory[pc++]; }

  static constexpr uint16_t xtr12_15 = (0b1111 << 12);
  static constexpr uint16_t xtr9_11 = (0b111 << 9);
  static constexpr uint16_t xtr0_8 = (0b111111111);
  static constexpr uint16_t xtr0_7 = (0b11111111);
  static constexpr uint16_t xtr6_8 = (0b111 << 6);
  static constexpr uint16_t xtr0_2 = (0b111);
  static constexpr uint16_t xtr0_3 = (0b1111);
  static constexpr uint16_t xtr0_4 = (0b11111);
  static constexpr uint16_t xtr0_5 = (0b111111);
  static constexpr uint16_t xtr5 = (0b1 << 5);
  static constexpr uint16_t xtr11 = (0b1 << 11);
  static constexpr uint16_t xtr0_10 = (0b11111111111);

  uint16_t curOpr() { return (ir & xtr12_15) >> 12; }
  uint16_t curReg0() { return (ir & xtr9_11) >> 9; }
  uint16_t curReg1() { return (ir & xtr6_8) >> 6; }
  uint16_t curReg2() { return (ir & xtr0_2); }
  uint16_t offset11() { return (ir & xtr0_10); }
  uint16_t offset9() { return (ir & xtr0_8); }
  uint16_t offset6() { return (ir & xtr0_5); }
  uint16_t imm5() { return (ir & xtr0_4); }
  uint16_t imm9() { return offset9(); }
  uint16_t code() { return curReg1(); }
  uint16_t flag6() { return (ir & xtr5) >> 5; }
  uint16_t flag12() { return (ir & xtr11) >> 11; }
  uint16_t code2() { return ir & xtr0_3; }

  void execute();
  void execAdd();
  void execLd();
  void execSt();
  void execJump();
  void execAnd();
  void execLdr();
  void execStr();
  void execCmp();
  void execNot();
  void execArith();
  void execSub();
  void execRet();
  void execMvi();
  void execLea();
  void execTrap();
  void execBranch();
};

} // namespace LCC

#endif /* MACHINE_H */
