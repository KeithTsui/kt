//===-- Interpreter.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#include "Machine.h"
#include <cstdint>
#include <iostream>

namespace LCC {} // namespace LCC

int main(int argc, char *argv[]) {

  if (argc != 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: Interpreter <input filename>\n");
    exit(1);
  }

  LCC::Machine::instance().load(argv[1]);
  LCC::Machine::instance().run();

  return 0;
}
