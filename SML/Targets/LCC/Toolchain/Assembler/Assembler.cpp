#include <cassert>
#include <cctype>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "LCC/Assembler/assembler.h"
#include "LCC/LCC.h"
#include "LCC/Utilities/liner.h"
#include "LCC/Utilities/utilities.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/Twine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Path.h"

namespace LCC {

void Assembler::Instruction::print() {
  std::string ops;
  for (auto op : operands) {
    ops += op.str() + ", ";
  }

  std::cout << "Instruction: " << oper.str() << " " << ops << '\n';
}

std::unique_ptr<Assembler> Assembler::make(std::string inputFilename,
                                           std::string outputFilename) {
  if (inputFilename.empty())
    return nullptr;
  auto inputFileExtension = llvm::sys::path::extension(inputFilename);
  if (inputFileExtension != LCC::assemblyExtension) {
    std::cerr << "not an assembly file of extension "
              << inputFileExtension.str() << std::endl;
    return nullptr;
  }
  if (!llvm::sys::fs::exists(inputFilename)) {
    std::cerr << "not exist " << inputFilename << '\n';
    return nullptr;
  }

  if (outputFilename.empty()) {
    auto oName = toSmallVector(inputFilename.c_str());
    llvm::sys::path::replace_extension(oName, LCC::executableExtension);
    outputFilename = toStringRef(oName).str();
  }

  return std::unique_ptr<Assembler>(
      new Assembler(inputFilename, outputFilename));
}

template <typename T>
std::ostream &operator<<(std::ostream &out,
                         llvm::SmallVectorImpl<T> const &msg) {
  for (auto &element : msg) {
    out << element;
  }
  return out;
}

Assembler::Assembler(std::string inputFilename, std::string outputFilename)
    : inputFilename{inputFilename}, outputFilename{outputFilename} {}

bool Assembler::run() {
  std::cout << "input: " << inputFilename << '\n'
            << "output: " << outputFilename << '\n';
  auto inputOrError = llvm::MemoryBuffer::getFile(inputFilename);
  if (!inputOrError) {
    std::cerr << "cannot open file " << std::endl;
    exit(1);
  }

  inputBuffer = std::move(inputOrError.get());

  output.reset(new llvm::raw_fd_ostream(outputFilename, EC));

  auto assemblies = inputBuffer->getBuffer();

  // fisrt pass to generate symbol table
  loadSymbolTable(assemblies);

  // second pass to generate binary into file
  auto result = binaryGen(assemblies);
  if (!result) {
    result = objectFileGen();
  }

  output->flush();
  output->close();

  return result;
}

llvm::SmallVector<char> Assembler::toSmallVector(char const *str) {
  llvm::SmallVector<char> ret;
  while (*str != '\0') {
    ret.push_back(*str++);
  }
  return ret;
}

llvm::StringRef Assembler::toStringRef(llvm::SmallVectorImpl<char> const &str) {
  return llvm::StringRef(str.data(), str.size());
}

llvm::StringRef Assembler::removeComment(llvm::StringRef const &line) {
  auto commentLoc = line.find_first_of(';');
  if (commentLoc != llvm::StringRef::npos) {
    return llvm::StringRef(line.begin(), commentLoc);
  }
  return line;
}

// ensure little endian for LCC architecture
void Assembler::writeHead(uint16_t word) {
  auto w = reinterpret_cast<unsigned char *>(&word);
  headerBuffer.push_back(*w++);
  headerBuffer.push_back(*w);
}

void Assembler::writeHead(llvm::StringRef str) {
  for (auto c : str) {
    uint16_t word = static_cast<uint16_t>(c);
    writeHead(word);
  }
  writeHead(toWord('\0')); // string should be ended with \0;
}

void Assembler::writeOut(uint16_t word) {
  auto w = reinterpret_cast<unsigned char *>(&word);
  codeBuffer.push_back(*w++);
  codeBuffer.push_back(*w);
}

void Assembler::writeOut(llvm::StringRef str) {
  for (auto c : str) {
    uint16_t word = static_cast<uint16_t>(c);
    writeOut(word);
  }
}

void Assembler::preserveSpace(size_t words) {
  while (words--) {
    writeOut(0);
  }
}

bool Assembler::is_number(const llvm::StringRef s) {
  return !s.empty() && std::find_if(s.begin(), s.end(), [](unsigned char c) {
                         return !std::isdigit(c);
                       }) == s.end();
}

void Assembler::loadSymbolTable(llvm::StringRef assemblies) {
  size_t currentAddress{0};
  size_t lineNumber{0};
  LCC::Liner liner{assemblies};
  symbolTable.clear();
  while (auto line = liner.nextLine()) {
    ++lineNumber;

    auto curLine = line.get();
    // remove comment in the line
    curLine = removeComment(curLine);
    curLine = curLine.ltrim().rtrim();

    if (curLine.empty())
      continue;

    // extract label if exists
    auto labelSize = curLine.find_first_of(':');
    if (labelSize != llvm::StringRef::npos) {
      auto label = llvm::StringRef(curLine.begin(), labelSize);
      symbolTable[label] = currentAddress;
      curLine = curLine.drop_front(labelSize + 1).ltrim();
    }

    if (curLine.empty())
      continue;

    // extract directive
    auto directiveBeginLoc = curLine.find_first_of('.');
    auto directiveEndLoc = curLine.find_first_of(' ', directiveBeginLoc);
    if (directiveBeginLoc != llvm::StringRef::npos &&
        directiveEndLoc != llvm::StringRef::npos) {
      auto directiveSize = directiveEndLoc - directiveBeginLoc;
      auto directive =
          llvm::StringRef(curLine.begin() + directiveBeginLoc, directiveSize);

      curLine = curLine.drop_front(directiveSize).ltrim();

      assert(!curLine.empty());

      // xx: .fill 6; xx: .fill label
      if (directive == ".fill" || directive == ".word") {
        ++currentAddress;
      } else if (directive == ".blkw") {
        if (std::isalpha(*curLine.begin())) {
          ++currentAddress;
        } else { // it's a number
          long converted = strtol(curLine.str().c_str(), NULL, 10);
          currentAddress += converted;
        }
      } else if (directive == ".stringz") {
        currentAddress += curLine.size();
      } else if (directive == ".start") {
      } else if (directive == ".global") {
      } else if (directive == ".extern") {
      } else if (directive == ".orig") {
        if (!std::isalpha(*curLine.begin())) {
          long converted = strtol(curLine.str().c_str(), NULL, 10);
          currentAddress = converted;
        }
      }
    } else {
      // it's an instruction
      ++currentAddress;
    }
  }
}

bool Assembler::binaryGen(llvm::StringRef assemblies) {

  size_t currentAddress{0};
  size_t lineNumber{0};
  LCC::Liner liner{assemblies};
  while (auto line = liner.nextLine()) {
    ++lineNumber;

    auto curLine = line.get();

    // remove comment in the line
    curLine = removeComment(curLine);
    curLine = curLine.ltrim().rtrim();

    if (curLine.empty())
      continue;

    // extract label if exists
    auto labelSize = curLine.find_first_of(':');
    if (labelSize != llvm::StringRef::npos) {
      curLine = curLine.drop_front(labelSize + 1).ltrim();
    }

    if (curLine.empty())
      continue;

    if (*curLine.begin() == '.') {
      // extract directive
      auto directiveBeginLoc = curLine.find_first_of('.');
      auto directiveEndLoc = curLine.find_first_of(' ', directiveBeginLoc);
      if (directiveBeginLoc != llvm::StringRef::npos &&
          directiveEndLoc != llvm::StringRef::npos) {
        auto directiveSize = directiveEndLoc - directiveBeginLoc;
        auto directive =
            llvm::StringRef(curLine.begin() + directiveBeginLoc, directiveSize);

        curLine = curLine.drop_front(directiveSize).ltrim();

        assert(!curLine.empty() && "error 01");

        // xx: .fill 6; xx: .fill label
        if (directive == ".fill" || directive == ".word") {
          if (std::isalpha(curLine.front())) { // it's a label
            uint16_t ret = 0;
            if (symbolTable.count(curLine)) { // local reference
              ret = static_cast<uint16_t>(symbolTable[curLine]);
              localReferenceTable[currentAddress] = curLine.str();
            } else { // external reference
              externalReferenceTable16bit[currentAddress] = curLine.str();
            }
            writeOut(ret);
          } else { // it's a number
            long converted = strtol(curLine.str().c_str(), NULL, 10);
            writeOut(static_cast<uint16_t>(converted));
          }
          ++currentAddress;
        } else if (directive == ".blkw") {
          if (std::isalpha(curLine.front())) { // it's a label
            std::cerr << "error on parsing .blkw directive\n";
            return true;
          } else { // it's a number
            long converted = strtol(curLine.str().c_str(), NULL, 10);
            preserveSpace(converted);
            currentAddress += converted;
          }
        } else if (directive == ".stringz") {
          // remove double quotation.
          writeOut(curLine.drop_front().drop_back());
          currentAddress += (curLine.size() * 2);
        } else if (directive == ".start") {
          startTable.push_back(symbolTable[curLine]);
        } else if (directive == ".global") {
          globalTable[curLine] = symbolTable[curLine];
        } else if (directive == ".extern") {
          externalTable[curLine] = 0;
        } else if (directive == ".orig") {
          if (!std::isalpha(curLine.front())) { // it's a number
            long converted = strtol(curLine.str().c_str(), NULL, 10);
            currentAddress = converted;
          }
        }
      }
    } else if (std::isalpha(*curLine.begin())) {
      // it's an instruction
      writeOut(parseInstruction(curLine, currentAddress));
      ++currentAddress;
    } else {
      // error
      std::cerr << "unexpected character " << *curLine.begin() << std::endl;
      return true;
    }
  }

  return false;
}

bool Assembler::objectFileGen() {

  // output file magic byte
  writeHead(LCC::magicByte);

  // output header
  // start table
  for (auto s : startTable) {
    writeHead('S');
    writeHead(s);
  }

  // global table
  for (auto &p : globalTable) {
    writeHead('G');
    writeHead(p.second);
    writeHead(p.first());
  }
  // externalreferencetable11bit
  for (auto &p : externalReferenceTable11bit) {
    writeHead('E');
    writeHead(p.first);
    writeHead(p.second);
  }
  // externalreferencetable16bit
  for (auto &p : externalReferenceTable16bit) {
    writeHead('V');
    writeHead(p.first);
    writeHead(p.second);
  }
  // externalreferencetable9bit
  for (auto &p : externalReferenceTable9bit) {
    writeHead('e');
    writeHead(p.first);
    writeHead(p.second);
  }
  // localReferencetable
  for (auto &p : localReferenceTable) {
    writeHead('A');
    writeHead(p.first);
    writeHead(p.second);
  }

  // output header-code sperator
  writeHead(LCC::headBodySeperator);

  // output header
  for (auto c : headerBuffer) {
    // uint8_t *ptr = reinterpret_cast<uint8_t *>(&c);
    // uint8_t first = *ptr;
    // uint8_t second = *(ptr + 1);
    // (*output) << first << second;
    // output->write(first);
    // output->write(second);
    (*output) << c;
  }

  // output code
  for (auto c : codeBuffer) {
    // uint8_t *ptr = reinterpret_cast<uint8_t *>(&c);
    // uint8_t first = *ptr;
    // uint8_t second = *(ptr + 1);
    // (*output) << first << second;
    (*output) << c;
    // output->write(first);
    // output->write(second);
  }

  output->flush();

  return false;
}

std::unique_ptr<Assembler::Instruction>
Assembler::Instruction::make(llvm::StringRef instruction) {
  instruction = instruction.ltrim().rtrim();

  auto splited = instruction.split(' ');
  auto cmd = splited.first;
  if (cmd.empty())
    return nullptr;

  Instruction *instr = new Instruction;
  instr->oper = cmd;

  auto rest = splited.second.ltrim();
  if (!rest.empty()) {
    std::vector<llvm::StringRef> operands;
    while (rest.count(',')) {
      splited = rest.split(',');
      operands.push_back(splited.first);
      rest = splited.second.ltrim();
    }
    operands.push_back(rest);
    instr->operands = std::move(operands);
  }

  return std::unique_ptr<Assembler::Instruction>(instr);
}

uint16_t Assembler::parseInstruction(llvm::StringRef instruction,
                                     size_t currentAddress) {

  auto instr = Instruction::make(instruction);
  instr->print();

  auto cmd = instr->oper;
  auto cmdStr = cmd.str();
  auto inst = instr.get();

  if (trapSet.count(cmdStr)) {
    return parseInstrTrap(inst, currentAddress);
  } else if (brSet.count(cmdStr)) {
    return parseInstrBr(inst, currentAddress);
  } else if (addSet.count(cmdStr)) {
    return parseInstrAdd(inst, currentAddress);
  } else if (cmd == "ld") {
    return parseInstrLd(inst, currentAddress);
  } else if (cmd == "st") {
    return parseInstrSt(inst, currentAddress);
  } else if (callSet.count(cmdStr)) {
    return parseInstrCall(inst, currentAddress);
  } else if (cmd == "and") {
    return parseInstrAnd(inst, currentAddress);
  } else if (cmd == "ldr") {
    return parseInstrLdr(inst, currentAddress);
  } else if (cmd == "str") {
    return parseInstrStr(inst, currentAddress);
  } else if (cmd == "cmp") {
    return parseInstrCmp(inst, currentAddress);
  } else if (cmd == "not") {
    return parseInstrNot(inst, currentAddress);
  } else if (memSet.count(cmdStr)) {
    return parseInstrMem(inst, currentAddress);
  } else if (cmd == "sub") {
    return parseInstrSub(inst, currentAddress);
  } else if (cmd == "jmp" || cmd == "ret") {
    return parseInstrRet(inst, currentAddress);
  } else if (cmd == "mvi") {
    return parseInstrMvi(inst, currentAddress);
  } else if (cmd == "lea") {
    return parseInstrLea(inst, currentAddress);
  } else {
    std::string msg = "unregconize asm ";
    msg += instruction;
    llvm_unreachable(msg.c_str());
  }

  return 0;
}

llvm::StringMap<uint8_t> Assembler::registerSet{
    {"r0", 0}, {"r1", 1}, {"r2", 2}, {"r3", 3}, {"r4", 4}, {"r5", 5},
    {"r6", 6}, {"r7", 7}, {"fp", 5}, {"sp", 6}, {"lr", 7}};

llvm::StringMap<uint16_t> Assembler::trapSet{
    {"halt", 0}, {"nl", 1},   {"dout", 2}, {"udout", 3}, {"hout", 4},
    {"aout", 5}, {"sout", 6}, {"din", 7},  {"hin", 8},   {"ain", 9},
    {"sin", 10}, {"m", 11},   {"r", 12},   {"s", 13},    {"bp", 14}};

uint16_t Assembler::parseInstrTrap(Instruction *instruction,
                                   size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 15 << 12;
  uint16_t reg{0};
  if (instruction->operands.size() > 0) {
    reg = (registerSet[instruction->operands[0]]) << 9;
  }
  uint16_t Code = trapSet[cmd];

  return ret | Code | reg;
}

llvm::StringMap<uint16_t> Assembler::brSet{
    {"brz", 0}, {"bre", 0}, {"brnz", 1}, {"brne", 1},
    {"brn", 2}, {"brp", 3}, {"brlt", 4}, {"brgt", 5},
    {"brc", 6}, {"brb", 6}, {"br", 7},   {"bral", 7}};

uint16_t Assembler::parseInstrBr(Instruction *instruction,
                                 size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 0 << 12;
  uint16_t offset = currentAddress + 1;
  if (instruction->operands.size() > 0) {
    auto op = instruction->operands[0];
    uint16_t addr = 0;
    if (symbolTable.count(op)) { // local reference
      addr = symbolTable[op];
      //localReferenceTable[currentAddress] = op.str();
      auto diff =
          static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 1);
      offset = convertToUInt16<int32_t>(diff, 9);
    } else if (externalTable.count(op)) { // external reference
      externalReferenceTable9bit[currentAddress] = op.str();
      offset = 0;
    } else {
      std::string msg = "symbol not found: ";
      msg += op;
      llvm_unreachable(msg.c_str());
    }

  }
  uint16_t code = brSet[cmd];

  return ret | (code << 9) | offset;
}

llvm::StringMap<uint16_t> Assembler::addSet{{"add", 1}};
uint16_t Assembler::parseInstrAdd(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 1 << 12;

  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = registerSet[op0];
  auto op1Id = registerSet[op1];
  uint16_t op2value = {0};
  if (registerSet.count(op2) == 1) {
    op2value = registerSet[op2];
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrLd(Instruction *instruction,
                                 size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 2 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = registerSet[op0];
  uint16_t opvalue = {0};

  uint16_t addr = 0;
  if (symbolTable.count(op1)) { // local reference
    addr = symbolTable[op1];
    //localReferenceTable[currentAddress] = op1.str();
    auto diff =
        static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 1);
    opvalue = convertToUInt16(diff, 9);
  } else if (externalTable.count(op1)) { // external reference
    externalReferenceTable9bit[currentAddress] = op1.str();
  } else {
    std::string msg = "symbol not found: ";
    msg += op1;
    llvm_unreachable(msg.c_str());
  }

  return ret | (op0Id << 9) | opvalue;
}

uint16_t Assembler::parseInstrSt(Instruction *instruction,
                                 size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 3 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = registerSet[op0];
  uint16_t opvalue = {0};

  uint16_t addr = 0;
  if (symbolTable.count(op1)) { // local reference
    addr = symbolTable[op1];
    //localReferenceTable[currentAddress] = op1.str();
    auto diff =
        static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 1);
    opvalue = convertToUInt16<int32_t>(diff, 9);
  } else if (externalTable.count(op1)) { // external reference
    externalReferenceTable9bit[currentAddress] = op1.str();
  } else {
    std::string msg = "symbol not found: ";
    msg += op1;
    llvm_unreachable(msg.c_str());
  }

  return ret | (op0Id << 9) | opvalue;
}

llvm::StringMap<uint16_t> Assembler::callSet{{"bl", 4 << 12},
                                             {"call", 4 << 12},
                                             {"jsr", 4 << 12},
                                             {"blr", 4 << 12},
                                             {"jsrr", 4 << 12}};

uint16_t Assembler::parseInstrCall(Instruction *instruction,
                                   size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 4 << 12;

  uint16_t opval = 0;
  uint16_t opval1 = 0;
  if (cmd == "bl" || cmd == "call" || cmd == "jsr") {
    auto op = instruction->operands[0];
    opval = 1 << 11;

    uint16_t addr = 0;
    if (symbolTable.count(op)) { // local reference
      addr = symbolTable[op];
      //localReferenceTable[currentAddress] = op.str();
      auto diff =
          static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 1);
      opval1 = convertToUInt16(diff, 11);
    } else if (externalTable.count(op)) { // external reference
      externalReferenceTable11bit[currentAddress] = op.str();
    } else {
      std::string msg = "symbol not found: ";
      msg += op;
      llvm_unreachable(msg.c_str());
    }

  } else if (cmd == "blr" || cmd == "jsrr") {
    auto op = instruction->operands[0];
    opval = registerSet[op] << 6;

    if (instruction->operands.size() == 2) {
      auto op1 = instruction->operands[1];
      auto addr = strtol(op1.str().c_str(), NULL, 10);
      opval1 = convertToUInt16(addr, 6);
    }

  } else {
    llvm_unreachable("there should one or two arguments in Call instruction.");
  }

  return ret | opval | opval1;
}

uint16_t Assembler::parseInstrAnd(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 5 << 12;

  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = registerSet[op0];
  auto op1Id = registerSet[op1];
  uint16_t op2value = {0};
  if (registerSet.count(op2) == 1) {
    op2value = registerSet[op2];
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrLdr(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 6 << 12;
  uint16_t opval = 0;
  uint16_t opval1 = 0;
  uint16_t opval2 = 0;
  if (instruction->operands.size() == 3) {
    auto op = instruction->operands[0];
    auto op1 = instruction->operands[1];
    auto op2 = instruction->operands[1];
    opval = registerSet[op] << 9;
    opval1 = registerSet[op1] << 6;

    auto x = strtol(op2.str().c_str(), NULL, 10);
    opval2 = convertToUInt16(x, 5);
  } else {
    llvm_unreachable("there should one or two arguments in ldr instruction.");
  }

  return ret | opval | opval1 | opval2;
}

uint16_t Assembler::parseInstrStr(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 7 << 12;
  uint16_t opval = 0;
  uint16_t opval1 = 0;
  uint16_t opval2 = 0;
  if (instruction->operands.size() == 3) {
    auto op = instruction->operands[0];
    auto op1 = instruction->operands[1];
    auto op2 = instruction->operands[1];
    opval = registerSet[op] << 9;
    opval1 = registerSet[op1] << 6;

    auto x = strtol(op2.str().c_str(), NULL, 10);
    opval2 = convertToUInt16(x, 5);
  } else {
    llvm_unreachable("there should one or two arguments in ldr instruction.");
  }

  return ret | opval | opval1 | opval2;
}

uint16_t Assembler::parseInstrCmp(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 8 << 12;

  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = registerSet[op0];
  auto op1Id = registerSet[op1];
  uint16_t op2value = {0};
  if (registerSet.count(op2) == 1) {
    op2value = registerSet[op2];
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrNot(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 9 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = registerSet[op0];
  auto op1Id = registerSet[op1];

  return ret | (op0Id << 9) | (op1Id << 6);
}

llvm::StringMap<uint16_t> Assembler::memSet{
    {"push", 0}, {"pop", 1},  {"srl", 2}, {"sra", 3}, {"sll", 4},
    {"rol", 5},  {"ror", 6},  {"mul", 7}, {"div", 8}, {"rem", 9},
    {"or", 10},  {"xor", 11}, {"mvr", 12}};
uint16_t Assembler::parseInstrMem(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 10 << 12;
  uint16_t reg0{0};
  uint16_t reg1{0};
  if (instruction->operands.size() == 1) {
    reg0 = (registerSet[instruction->operands[0]]) << 9;
  } else if (instruction->operands.size() == 2) {
    reg0 = (registerSet[instruction->operands[0]]) << 9;
    reg1 = (registerSet[instruction->operands[1]]) << 6;
  } else {
    llvm_unreachable("number of arguments is wrong.");
  }
  uint16_t code = memSet[cmd];

  return ret | reg0 | reg1 | code;
}

uint16_t Assembler::parseInstrSub(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 11 << 12;

  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = registerSet[op0];
  auto op1Id = registerSet[op1];
  uint16_t op2value = {0};
  if (registerSet.count(op2) == 1) {
    op2value = registerSet[op2];
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrRet(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 12 << 12;

  uint16_t code = 0b111 << 9;
  uint16_t baser = 0b111 << 6;
  uint16_t offset = 0;
  if (instruction->operands.size() == 2) {
    auto op = instruction->operands[0];
    auto op1 = instruction->operands[1];
    baser = registerSet[op] << 6;
    auto x = strtol(op1.str().c_str(), NULL, 10);
    offset = convertToUInt16(x, 9);

  } else if (instruction->operands.size() == 1) {
    auto op = instruction->operands[0];
    auto x = strtol(op.str().c_str(), NULL, 10);
    offset = convertToUInt16(x, 9);

  } else if (instruction->operands.size() == 0) {
    // do nothing
  } else {
    llvm_unreachable("there should one or two arguments in Call instruction.");
  }

  return ret | code | baser | offset;
}

uint16_t Assembler::parseInstrMvi(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 13 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = registerSet[op0];
  uint16_t opvalue = {0};
  auto x = strtol(op1.str().c_str(), NULL, 10);
  opvalue = convertToUInt16(x, 9);

  return ret | (op0Id << 9) | opvalue;
}

uint16_t Assembler::parseInstrLea(Instruction *instruction,
                                  size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 14 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for lea instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = registerSet[op0];
  uint16_t opvalue = {0};

  uint16_t addr = 0;
  if (symbolTable.count(op1)) { // local reference
    addr = symbolTable[op1];
    //localReferenceTable[currentAddress] = op1.str();
    auto diff =
        static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 1);
    opvalue = convertToUInt16<int32_t>(diff, 9);
  } else if (externalTable.count(op1)) { // external reference
    externalReferenceTable9bit[currentAddress] = op1.str();
  } else {
    std::string msg = "symbol not found: ";
    msg += op1;
    llvm_unreachable(msg.c_str());
  }



  return ret | (op0Id << 9) | opvalue;
}

} // namespace LCC
