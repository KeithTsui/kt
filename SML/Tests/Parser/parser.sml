structure PT = struct

structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
structure Lex = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
structure TigerP = Join(structure ParserData = TigerLrVals.ParserData
                        structure Lex = Lex
                        structure LrParser = LrParser)

fun lexer filename =
    let val _ = (ErrorMsg.reset(); ErrorMsg.fileName := filename)
        val file = TextIO.openIn filename
        fun get _ = TextIO.input file
        fun parseerror(s,p1,p2) = ErrorMsg.error p1 s
    in
        Lex.makeLexer get
    end

fun parse filename =
    let val _ = (ErrorMsg.reset(); ErrorMsg.fileName := filename)
        val file = TextIO.openIn filename
        fun get _ = TextIO.input file
        fun parseerror(s,p1,p2) = ErrorMsg.error p1 s
        val lexer = LrParser.Stream.streamify (Lex.makeLexer get)
        val (result, _) = TigerP.parse(0, lexer, parseerror, ())
    in
        (
          TextIO.closeIn file;
          print(filename ^ ":\n");
          ASTPrinter.print(TextIO.stdOut, result);
          print("\n")
        )
    end
    handle LrParser.ParseError
           => raise LrParser.ParseError

fun term2string (LrParser.Token.TOKEN(t, v)) =
    TigerLrVals.ParserData.EC.showTerminal t

fun showLexer lx =
    case term2string(lx()) of
        "EOF" => print " EOF\n "
      | s => (print (" " ^ s ^ " "); showLexer lx)

fun lexing filename = showLexer (lexer filename)

fun testfiles 0 = "test" ^ Int.toString 0 ^ ".tig" :: nil
  | testfiles n = "test" ^ Int.toString n ^ ".tig" :: testfiles (n - 1)

fun println str = print (str ^ "\n")

fun testfile1() =
    "/Users/k/Dev/TigerCompiler/KT/Tests/TigerCodeTestCases/test0.tig"

fun testLexer() = showLexer (lexer "test1.tig")

fun test1() =
    let val tf = testfile1()
    in
        (lexing tf;
         parse tf
        )
    end

exception TestError of string

fun testfile f =
    (lexing f; parse f)
    handle LrParser.ParseError => raise TestError(f)

fun testAll() =
    let
        val testfileDir = "/Users/k/Dev/TigerCompiler/KT/Tests/TigerCodeTestCases/";
        val testfiles = rev (map (fn tf => testfileDir ^ tf) (testfiles 49))
    in
        (map println testfiles;
         map testfile testfiles
        )
    end
end
