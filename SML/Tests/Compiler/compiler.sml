structure CT = struct

(*
structure mipsTigerCompiler = CompilerFunctor(MipsGen)
*)

structure lccTigerCompiler = CompilerFunctor(LCCGen)

fun testfiles 0 = "test" ^ Int.toString 0 ^ ".tig" :: nil
  | testfiles n = "test" ^ Int.toString n ^ ".tig" :: testfiles (n - 1)

fun test() =
    lccTigerCompiler.compile "test.tig"

fun testAll() =
    let val testfileDir = "/Users/k/Dev/TigerCompiler/KT/Tests/TigerCodeTestCases/";
        val testfiles = rev (map (fn tf => testfileDir ^ tf) (testfiles 49))
    in
        (*
        app mipsTigerCompiler.compile testfiles; 0
        *)
        app lccTigerCompiler.compile testfiles; 0
    end

end
