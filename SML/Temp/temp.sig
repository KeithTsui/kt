(*
Temporary
the word temporary to mean a value that is temporarily held in a register,
the word label to mean some machine-language location whose exact address is yet to be determined.
*)

signature TEMP =
sig
    eqtype temp
    val newtemp : unit -> temp
    structure Table : TABLE sharing type Table.key = temp
    val makestring: temp -> string

    type label = Symbol.symbol
    val newlabel : unit -> label
    val namedlabel : string -> label
end
