# Keith Tiger Compiler (KT)
A compiler study project from programming language to instruction execution.

## Welcome to Keith Tiger Compiler.
Tiger programming language is a programming language for study in [Modern Compiler Implementation in ML](https://www.amazon.com/Modern-Compiler-Implement-Andrew-Appel/dp/0521607647).
Keith Tiger Compiler is an implementation of Tiger Programming language as well.

Although there are many implementations, the purpose of this project is to study how compiler works and down to the ISA part, and group each component as a library to make those component more modulable. One benefit of modulization is that it is easy to write a new backend for Tiger.

In this project, there are two backend, MIPS and LCC.


# LCC
LCC is a computer architecture in [C and C++ under the hood](https://us.amazon.com/C-Under-Hood-2nd/dp/B09B74P6C4). In this book, the author offers toolchain in executable form for LCC, which contains Assembler, Linker, and Simulator, but no source code for the toolchain.

Therefore in the LCC backend of this project, I implement LCC toolchain in C++ as well. In order to close the loop of Programming language, Compiler, Assembler, Linker, and down to the Simulator, which means we can compile a Tiger program and execute on a LCC architecture in Simulator. And the whole process is source code available. So you can check out how each part works.


# Demo
1. Install `SML-NJ`.
```bash
$ brew install smlnj
```

2. Run LCC compiler to compile following code in Tiger Language and output LCC asm file.
```
let
  var arr1:int := 8
in
  printi(arr1 + 10)
end
```

```bash
$ git clone git@gitlab.com:KeithTsui/kt.git KT
$ cd KT/SML/Tests/Compiler
$ sml
- CM.make "compiler.cm";
- CT.test();
- OS.Process.exit(OS.Process.success);
```
Now, you can see `test.tig.a` at current directory. Then let us assemble it.

```bash
$ sudo chmod +x ./lcc
$./lcc test.tig.a
$./lcc startup.o printi.o test.tig.o
$./lcc link.e                       
Starting interpretation of link.e
lst file = link.lst
bst file = link.bst
====================================================== Output
18

```
